package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

type DadJokes struct {
	ID     string `json:"id"`
	Joke   string `json:"joke"`
	Status int    `json:"status"`
}

func main() {
	app := fiber.New()
	client := &http.Client{}
	var errorMessage interface{}

	req, err := http.NewRequest("GET", "https://icanhazdadjoke.com/", nil)
	if err != nil {
		errorMessage = err.Error()
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")

	res, errRes := client.Do(req)
	if errRes != nil {
		log.Fatal(err, "res")
	}
	defer res.Body.Close()
	bodyBytes, errBody := ioutil.ReadAll(res.Body)
	if errBody != nil {
		log.Fatal(err, "body")
	}

	var data DadJokes
	json.Unmarshal(bodyBytes, &data)

	app.Get("/jokes", func(c *fiber.Ctx) error {

		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"id":       data.ID,
			"status":   data.Status,
			"The Joke": data.Joke,
			"error":    errorMessage,
		})
	})

	app.Listen(":3000")
}
